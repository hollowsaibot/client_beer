package org.udg.pds.pds_android.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.widget.Toast;
import com.thoughtworks.xstream.XStream;
import org.udg.pds.pds_android.activity.UsuarisActivity;
import org.udg.pds.pds_android.data.UsuariListREST;
import org.udg.pds.pds_android.data.UsuariListREST.UsuariREST;
import org.udg.pds.pds_android.service.RESTService;


public class RESTResponderUsuari extends Fragment {
    private static String TAG = RESTResponderUsuari.class.getName();

    private ResultReceiver mReceiver;
    private UsuariListREST mUsuaris;

    public RESTResponderUsuari() {
        mReceiver = new ResultReceiver(new Handler()) {

            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultData != null && resultData.containsKey(RESTService.REST_RESULT)) {
                    onRESTResult(resultCode, resultData.getString(RESTService.REST_RESULT));
                }
                else {
                    onRESTResult(resultCode, null);
                }
            }

        };
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // This tells our Activity to keep the same instance of this
        // Fragment when the Activity is re-created during lifecycle
        // events. This is what we want because this Fragment should
        // be available to receive results from our RESTService no
        // matter what the Activity is doing.
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // This gets called each time our Activity has finished creating itself.
        setTweets();
    }

    private void setTweets() {
        UsuarisActivity activity = (UsuarisActivity) getActivity();

        if (mUsuaris == null && activity != null) {
            // This is where we make our REST call to the service. We also pass in our ResultReceiver
            // defined in the RESTResponderFragment super class.

            // We will explicitly call our Service since we probably want to keep it as a private
            // component in our app. You could do this with Intent actions as well, but you have
            // to make sure you define your intent filters correctly in your manifest.
            Intent intent = new Intent(activity, RESTService.class);

            //Obtenir informacio pasada entre activitats
            // Estem en fragment
            String rec_user_alias = getArguments().getString("user_alias");
            String rec_user_pass = getArguments().getString("user_pass");

            // IMPORTANT: canvieu l'URL depenent de si voleu treballar amb el servidor local o el OpenShift
            //intent.setData(Uri.parse("https://project2-pdsudg.rhcloud.com/rest/Usuaris"));
            intent.setData(Uri.parse("http://10.0.2.2:8080/rest/usuaris/validar?alies="+rec_user_alias+"&pass="+rec_user_pass));


            // Here we are going to place our REST call parameters. Note that
            // we could have just used Uri.Builder and appendQueryParameter()
            // here, but I wanted to illustrate how to use the Bundle params.

            // Bundle params = new Bundle();

            intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, mReceiver);

            // Here we send our Intent to our RESTService.
            activity.startService(intent);
        }
        else if (activity != null) {
            // Here we check to see if our activity is null or not.
            // We only want to update our views if our activity exists.

            /*UsuarisAdapter adapter = activity.getUsuarisAdapter();

            // Load our list adapter with our Tweets.
            adapter.clear();
            for (UsuariREST m : mUsuaris.getUsuarisList()) {
                adapter.add(m);
            }*/
        }
    }

    public void onRESTResult(int code, String result) {
        // Here is where we handle our REST response. This is similar to the 
        // LoaderCallbacks<D>.onLoadFinished() call from the previous tutorial.

        // Check to see if we got an HTTP 200 code and have some data.
        if (code == 200 && result != null) {

            //mUsuaris = getUsuarisFromXML(result);
            Activity activity = getActivity();
            if (activity != null) {
                Toast.makeText(activity, "Usuari verificat :) \n "+result, Toast.LENGTH_LONG).show();
            }
            //setTweets();
        }
        else {
            Activity activity = getActivity();
            if (activity != null) {
                Toast.makeText(activity, "Failed to load Usuari data. Check your internet settings.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private static UsuariListREST getUsuarisFromXML(String xml) {
        XStream xstream = new XStream(); // does not require XPP3 library starting with Java 6
        xstream.processAnnotations(UsuariListREST.class);
        xstream.processAnnotations(UsuariREST.class);
        UsuariListREST UsuariList = (UsuariListREST) xstream.fromXML(xml);

        return UsuariList;
    }

}