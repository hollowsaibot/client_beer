package org.udg.pds.pds_android.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import com.thoughtworks.xstream.XStream;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.activity.*;
import org.udg.pds.pds_android.data.UsuariListREST;
import org.udg.pds.pds_android.service.RESTService;
import org.udg.pds.pds_android.xml.XMLCorrecte;
import org.udg.pds.pds_android.xml.XMLError;

/**
* Created with IntelliJ IDEA.
* User: imartin
* Date: 28/03/13
* Time: 15:49
* To change this template use File | Settings | File Templates.
*/


/**
* This Fragment contains the Tasks list layout
*/
public class SendBeer extends Fragment {
    private static String TAG = SendBeer.class.getName();

    public interface OnSendBeerListener
    {
        public void OnErrorSendBeer(XMLError e);
        public void OnSuccefulSendBeer(XMLCorrecte c);
    }

    private ResultReceiver mReceiver;

    public SendBeer() {
        mReceiver = new ResultReceiver(new Handler()) {

            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultData != null && resultData.containsKey(RESTService.REST_RESULT)) {
                    onRESTResult(resultCode, resultData.getString(RESTService.REST_RESULT));
                }
                else {
                    onRESTResult(resultCode, null);
                }
            }

        };
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // This tells our Activity to keep the same instance of this
        // Fragment when the Activity is re-created during lifecycle
        // events. This is what we want because this Fragment should
        // be available to receive results from our RESTService no
        // matter what the Activity is doing.
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // This gets called each time our Activity has finished creating itself.
        sendBeerAtAdmin();
    }

    private void sendBeerAtAdmin() {
         Activity activity = getActivity();

        if (activity != null) {
            // This is where we make our REST call to the service. We also pass in our ResultReceiver
            // defined in the RESTResponderFragment super class.

            // We will explicitly call our Service since we probably want to keep it as a private
            // component in our app. You could do this with Intent actions as well, but you have
            // to make sure you define your intent filters correctly in your manifest.
            Intent intent = new Intent(activity, RESTService.class);



            // IMPORTANT: canvieu l'URL depenent de si voleu treballar amb el servidor local o el OpenShift
            //intent.setData(Uri.parse("https://project2-pdsudg.rhcloud.com/rest/Usuaris"));
            intent.setData(Uri.parse("http://10.0.2.2:8080/rest/beers/add"));


            // Here we are going to place our REST call parameters. Note that
            // we could have just used Uri.Builder and appendQueryParameter()
            // here, but I wanted to illustrate how to use the Bundle params.

            // Bundle params = new Bundle();
            intent.putExtras(this.getArguments());

            intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, mReceiver);

            // Here we send our Intent to our RESTService.
            activity.startService(intent);
        }
    }

    public void onRESTResult(int code, String result) {
        // Here is where we handle our REST response. This is similar to the
        // LoaderCallbacks<D>.onLoadFinished() call from the previous tutorial.

        // Check to see if we got an HTTP 200 code and have some data.
        if (code == 200 && result != null) {

            OnSendBeerListener mCallback = ((OnSendBeerListener)getActivity());
            try
            {
                XMLCorrecte c = getCorrecteFromXML(result);
                mCallback.OnSuccefulSendBeer(c);
            }
            catch (Exception ex)
            {
                XMLError e = getErrorFromXML(result);
                mCallback.OnErrorSendBeer(e);
            }
        }
        else {
            Activity activity = getActivity();
            if (activity != null) {
                Toast.makeText(activity, "Failed to load Usuari data. Check your internet settings.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private static XMLCorrecte getCorrecteFromXML(String xml) {
        XStream xstream = new XStream(); // does not require XPP3 library starting with Java 6
        xstream.processAnnotations(XMLCorrecte.class);
        XMLCorrecte error = (XMLCorrecte) xstream.fromXML(xml);

        return error;
    }

    private static XMLError getErrorFromXML(String xml) {
        XStream xstream = new XStream(); // does not require XPP3 library starting with Java 6
        xstream.processAnnotations(XMLError.class);
        XMLError error = (XMLError) xstream.fromXML(xml);

        return error;
    }

}