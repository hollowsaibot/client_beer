package org.udg.pds.pds_android.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.thoughtworks.xstream.XStream;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.adapter.ComentariAdapter;
import org.udg.pds.pds_android.service.RESTService;
import org.udg.pds.pds_android.xml.XMLCommentList;
import org.udg.pds.pds_android.xml.XMLError;

import java.util.List;

/**
 * This Fragment contains the Tasks list layout
 */
public class BeerComments extends ListFragment {

    private ResultReceiver mReceiver;
    private XMLCommentList mComentaris;

    // This is the interface that the container activity has to implement
    // in order to use this fragment
    public interface OnBeerCommentsListener {
        public void updateBeerComments();
        public void onErrorComments(XMLError e);
    }
    // We will use this to call the container activity on response from REST calls
    private OnBeerCommentsListener mCallback;

    // This is the adapter responsible to show the list
    private ComentariAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.comment_list, container, false);
        /*Button b = (Button) view.findViewById(R.id.add_task_button);
        // This is the listener to the "Add Task" button
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // When we press the "Add Task" button, the AddTask activity is called, where
                // we can introduce the data of the new task
                Intent i = new Intent(getActivity(), AddTask.class);
                // We launch the activity with startActivityForResult because we want to know when
                // the launched activity has finished. In this case, when the AddTask activity has finished
                // we will update the list to show the new task.
                startActivityForResult(i, Global.RQ_ADD_TASK);
            }
        }); */

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // This tells our Activity to keep the same instance of this
        // Fragment when the Activity is re-created during lifecycle
        // events.
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Since onActivityCreated can be called multiple times in the Fragment lifecycle, we only initialize the
        // adapter if it is null (the first time)
        if (mAdapter == null) {
            mAdapter = new ComentariAdapter(getActivity(), R.layout.comment_layout);
            setListAdapter(mAdapter);
        }

        // This gets called each time our Activity has finished creating itself.
        //getComments();
        mCallback.updateBeerComments();
    }

    @Override
    public void onAttach(Activity a) {
        super.onAttach(a);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnBeerCommentsListener) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString()
                    + " must implement OnBeerCommentsListener");
        }
    }

    // This function is called from the container activity whenever it has received
    // a new task list from a REST responder
    public void showBeerComments(XMLCommentList bl) {
        // Load our list adapter with our Tasks. That would cause and automatic update
        // of the corresponding ListView
        mAdapter.clear();
        List<XMLCommentList.XMLComment> bn = bl.getCommentList();
        if(bn != null){

            for (XMLCommentList.XMLComment b : bn) {
                mAdapter.add(b);
            }
        }
    }

    public void getComments() {

        Activity activity = getActivity();

        //mComentaris == null &&
        if (activity != null) {
            // This is where we make our REST call to the service. We also pass in our ResultReceiver
            // defined in the RESTResponderFragment super class.

            int id_cervesa;
            Bundle bundle = activity.getIntent().getExtras();
            if(bundle.getInt("idCervesa")>0)
            {
                id_cervesa =  bundle.getInt("idCervesa");
                // We will explicitly call our Service since we probably want to keep it as a private
                // component in our app. You could do this with Intent actions as well, but you have
                // to make sure you define your intent filters correctly in your manifest.
                Intent intent = new Intent(activity, RESTService.class);

                // IMPORTANT: canvieu l'URL depenent de si voleu treballar amb el servidor local o el OpenShift
                //intent.setData(Uri.parse("https://project2-pdsudg.rhcloud.com/rest/Usuaris"));
                intent.setData(Uri.parse("http://10.0.2.2:8080/rest/beers/comments/"+id_cervesa));

                // Here we are going to place our REST call parameters. Note that
                // we could have just used Uri.Builder and appendQueryParameter()
                // here, but I wanted to illustrate how to use the Bundle params.

                // Bundle params = new Bundle();
                intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, RESTService.GET);

                // Finally we put a ResultReceiver into the intent, that the RESTService
                // will use to send back the results
                intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, new ResultReceiver(new Handler()) {
                    @Override
                    protected void onReceiveResult(int resultCode, Bundle resultData) {
                        String xml = resultData.getString(RESTService.REST_RESULT);
                        onRESTResult(resultCode, xml);
                    }});

                // Here we send our Intent to our RESTService.
                activity.startService(intent);
            }
        }
    }

    public void onRESTResult(int code, String result) {
        // Here is where we handle our REST response. This is similar to the 
        // LoaderCallbacks<D>.onLoadFinished() call from the previous tutorial.

        // Check to see if we got an HTTP 200 code and have some data.
        if (code == 200 && result != null) {
            try {
                mComentaris = getCommentsFromXML(result);
                showBeerComments(mComentaris);
            } catch (Exception ex) {
                // There has been an error
                XMLError e = getErrorFromXML(result);
                mCallback.onErrorComments(e);
            }
        }
        else {
            Activity activity = getActivity();
            if (activity != null) {
                Toast.makeText(activity, "Failed to load Comments data. Check your internet settings.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private static XMLCommentList getCommentsFromXML(String xml) {
        XStream xstream = new XStream(); // does not require XPP3 library starting with Java 6
        xstream.processAnnotations(XMLCommentList.class);
        xstream.processAnnotations(XMLCommentList.XMLComment.class);
        XMLCommentList ComentariList = (XMLCommentList) xstream.fromXML(xml);

        return ComentariList;
    }

    private static XMLError getErrorFromXML(String xml)
    {
        XStream xstream = new XStream(); // does not require XPP3 library starting with Java 6
        xstream.processAnnotations(XMLError.class);
        XMLError error = (XMLError) xstream.fromXML(xml);

        return error;
    }


}