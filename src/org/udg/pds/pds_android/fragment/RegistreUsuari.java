package org.udg.pds.pds_android.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.thoughtworks.xstream.XStream;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.activity.MainActivity;
import org.udg.pds.pds_android.activity.PerfilUsuariActivity;
import org.udg.pds.pds_android.adapter.BeersAdapter;
import org.udg.pds.pds_android.data.BeerListREST;
import org.udg.pds.pds_android.service.RESTService;
import org.udg.pds.pds_android.util.Global;
import org.udg.pds.pds_android.xml.XMLCorrecte;
import org.udg.pds.pds_android.xml.XMLError;
import org.udg.pds.pds_android.xml.XMLUsuari;

/**
 * Created with IntelliJ IDEA.
 * User: Norbert
 * Date: 24/04/13
 * Time: 18:34
 * To change this template use File | Settings | File Templates.
 */
public class RegistreUsuari extends Fragment{

    public RegistreUsuari(){
        mXstream.processAnnotations(XMLError.class);
        mXstream.processAnnotations(XMLCorrecte.class);
    }
    // This is the interface that the container activity has to implement
    // in order to use this fragment
    public interface OnRegister {
        public void Registrarse(String alies, String pass, String passConfirmacio, String email);
        public void onError(XMLError e);
    }
    // We will use this to call the container activity on response from REST calls
    private OnRegister mRegister;
    // This is the XStream objet that we will use to comvert XML responses into objects
    private XStream mXstream = new XStream();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.register_user, container, false);
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Activity a = getActivity();

        if (a!=null)
        {
            Button b = (Button) a.findViewById(R.id.btnSignUp);
            // This is teh listener that will be used when the user presses the "Login" button
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText alies = (EditText) a.findViewById(R.id.txtUserNickname);
                    EditText contrasenya = (EditText) a.findViewById(R.id.txtUserPassword);
                    EditText contrasenyaVerificacio = (EditText) a.findViewById(R.id.txtUserConfirmPassword);
                    EditText email = (EditText) a.findViewById(R.id.txtUserEmail);


                    if (alies.getText().length() <= 0)
                        Toast.makeText(getActivity(), "Has d'entrar algun alies.", Toast.LENGTH_LONG).show();
                    else if (contrasenya.getText().toString().length() <= 0) Toast.makeText(getActivity(), "Has d'entrar alguna contrasenya.", Toast.LENGTH_LONG).show();
                    else if (contrasenyaVerificacio.getText().toString().length() <= 0) Toast.makeText(getActivity(), "Has d'entrar alguna contrasenya de confirmació.", Toast.LENGTH_LONG).show();
                    else if (contrasenya.getText().toString().compareTo(contrasenyaVerificacio.getText().toString())!=0) Toast.makeText(getActivity(), "Les contrasenyes no coincideixen.", Toast.LENGTH_LONG).show();
                    else mRegister.Registrarse(alies.getText().toString(),contrasenya.getText().toString(),contrasenyaVerificacio.getText().toString(),email.getText().toString());

                }
            });
        }
    }

    @Override
    public void onAttach(Activity a) {
        super.onAttach(a);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mRegister = (OnRegister) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString()
                    + " must implement OnRegister");
        }
    }

    public void ServerRegister(String alies, String pass, String passConfirmacio, String email)
    {
        Activity a = getActivity();
        Intent intent = new Intent(a, RESTService.class);

        // Build the URL with the appropriate resource path
        intent.setData(Uri.parse(Global.BASE_URL + "/rest/users/register"));
        // Set the HTTP verb
        intent.putExtra(RESTService.EXTRA_HTTP_VERB, RESTService.POST);

        // Send the parameters for the POST request in a Bundle
        Bundle params = new Bundle();
        params.putString("username", alies);
        params.putString("password", pass);
        params.putString("email", email);
        intent.putExtra(RESTService.EXTRA_PARAMS, params);

        // Put a receiver in the intent to process the results
        intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, new ResultReceiver(new Handler()) {
            // This function will be called from the REST Service when it invokes the
            // send() command of the ResultReceiver
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultCode == 200) {
                    String xml = resultData.getString(RESTService.REST_RESULT);
                    try {
                        XMLCorrecte e = (XMLCorrecte) mXstream.fromXML(xml);
                        if (getActivity()!=null)    {
                            Toast.makeText(getActivity(), e.msg, Toast.LENGTH_LONG).show();
                            //Intent intent = new Intent(getActivity(), MainActivity.class);
                            //startActivity(intent);
                            getActivity().finish();
                        }
                    } catch (Exception ex) {
                        // There has been an error
                        XMLError e = (XMLError) mXstream.fromXML(xml);
                        mRegister.onError(e);
                    }
                }
            }
        });

        // Here we send our Intent to our RESTService.
        a.startService(intent);

    }
}
