package org.udg.pds.pds_android.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import com.thoughtworks.xstream.XStream;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.activity.AmistatsActivity;
import org.udg.pds.pds_android.activity.InvitacionsActivity;
import org.udg.pds.pds_android.activity.UsuariViewActivity;
import org.udg.pds.pds_android.adapter.UsuarisAdapter;
import org.udg.pds.pds_android.data.UsuariListREST;
import org.udg.pds.pds_android.service.RESTService;
import org.udg.pds.pds_android.util.Global;
import org.udg.pds.pds_android.xml.XMLError;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Norbert
 * Date: 08/05/13
 * Time: 02:19
 * To change this template use File | Settings | File Templates.
 */
public class Amistats extends ListFragment {

    // This is the public interface that the container activity has to implement
    // in order to use this fragment
    public interface onAmistatsListener {
        public void onError(XMLError e);
        public void onAmistatsListUpdate(UsuariListREST ul);
        public void updateAmistatList();
    }

    private onAmistatsListener mCallback;
    private UsuarisAdapter mAdapter;
    private UsuariListREST mUsuaris;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.amistats, container, false);
        return view;
    }

   @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //getAmistats();
        if (mAdapter == null) {
            mAdapter = new UsuarisAdapter(getActivity(), R.layout.user_layout);
            setListAdapter(mAdapter);
        }
        mCallback.updateAmistatList();
    }

    @Override
    public void onAttach(Activity a) {
        super.onAttach(a);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (onAmistatsListener) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }



    // This is the function used to retrieve all the tasks from an user from the server
    public void getAmistats() {
        Activity activity =  getActivity();

        if (mUsuaris == null && activity != null) {
            // This is where we make our REST call to the service. We also pass in our ResultReceiver
            // defined in the RESTResponderFragment super class.

            // We will explicitly call our Service since we probably want to keep it as a private
            // component in our app. You could do this with Intent actions as well, but you have
            // to make sure you define your intent filters correctly in your manifest.
            Intent intent = new Intent(activity, RESTService.class);

            // Set the resource URL
            intent.setData(Uri.parse(Global.BASE_URL + "/rest/users/friends"));
            // Set the HTTP verb
            intent.putExtra(RESTService.EXTRA_HTTP_VERB, RESTService.GET);

            // Finally we put a ResultReceiver into the intent, that the RESTService
            // will use to send back the results
            intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, new ResultReceiver(new Handler()) {
                @Override
                protected void onReceiveResult(int resultCode, Bundle resultData) {
                    if (resultCode == 200) {
                        String xml = resultData.getString(RESTService.REST_RESULT);
                        try {
                            UsuariListREST ul = (UsuariListREST) getUsuarisFromXML(xml);
                            mCallback.onAmistatsListUpdate(ul);
                        } catch (Exception ex) {
                            // There has been an error
                            XMLError e = (XMLError) getErrorFromXML(xml);
                            mCallback.onError(e);
                        }
                    }
                }});

            // Here we send our Intent to our RESTService.
            activity.startService(intent);
        }
    }

    // This function is called from the container activity whenever it has received
    // a new task list from a REST responder
    public void showUsuariList(UsuariListREST ulr) {
        // Load our list adapter with our Tasks. That would cause and automatic update
        // of the corresponding ListView

        AmistatsActivity activity = (AmistatsActivity)getActivity();
        if (activity!=null)
        {
            //mAdapter=activity.getUsuarisAdapter();
            mAdapter.clear();
            List<UsuariListREST.UsuariREST> lu = ulr.getUsuarisList();
            if(lu != null){
                for (UsuariListREST.UsuariREST u : lu) {
                    mAdapter.add(u);
                }
            }
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        // Do something with the data
        Toast.makeText(getActivity(),
                "Click ListItem Number " + position, Toast.LENGTH_LONG)
                .show();
        Intent i = new Intent(getActivity(), UsuariViewActivity.class);
        //TextView tv = v.findViewById(R.layout.)
        i.putExtra("idUsuari",v.getId()); //TODO: falta modificar
        i.putExtra("invitacio", 0); //TODO: falta modificar
        startActivity(i);
    }

    private static UsuariListREST getUsuarisFromXML(String xml) {
        XStream xstream = new XStream(); // does not require XPP3 library starting with Java 6
        xstream.processAnnotations(UsuariListREST.class);
        xstream.processAnnotations(UsuariListREST.UsuariREST.class);
        UsuariListREST usuarisList = (UsuariListREST) xstream.fromXML(xml);

        return usuarisList;
    }

    private static XMLError getErrorFromXML(String xml)
    {
        XStream xstream = new XStream(); // does not require XPP3 library starting with Java 6
        xstream.processAnnotations(XMLError.class);
        XMLError error = (XMLError) xstream.fromXML(xml);

        return error;
    }


}
