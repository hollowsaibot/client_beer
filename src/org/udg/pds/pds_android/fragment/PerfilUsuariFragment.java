package org.udg.pds.pds_android.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.*;
import com.thoughtworks.xstream.XStream;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.activity.PerfilUsuariActivity;
import org.udg.pds.pds_android.data.UsuariListREST;
import org.udg.pds.pds_android.data.UsuariListREST.UsuariREST;
import org.udg.pds.pds_android.service.RESTService;
import org.udg.pds.pds_android.util.Global;
import org.udg.pds.pds_android.xml.XMLCorrecte;
import org.udg.pds.pds_android.xml.XMLError;

/**
 * Created with IntelliJ IDEA.
 * User: Norbert
 * Date: 19/04/13
 * Time: 01:05
 * To change this template use File | Settings | File Templates.
 */
public class PerfilUsuariFragment extends Fragment {
    private static String TAG = PerfilUsuariFragment.class.getName();

    private ResultReceiver mReceiver;
    private UsuariREST mUsuaris;
    private XStream mXstream = new XStream();

    int opcioBoto = 0;

    public PerfilUsuariFragment() {
        mReceiver = new ResultReceiver(new Handler()) {

            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultData != null && resultData.containsKey(RESTService.REST_RESULT)) {
                    onRESTResult(resultCode, resultData.getString(RESTService.REST_RESULT));
                }
                else {
                    onRESTResult(resultCode, null);
                }
            }
        };
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // This gets called each time our Activity has finished creating itself.
        setUser();

        Button b = (Button) getActivity().findViewById(R.id.btnInvite);
        // This is teh listener that will be used when the user presses the "Login" button
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (opcioBoto==0) acceptarInvitacio();
                else if (opcioBoto==1) ferPeticioAmistat();
            }
        });

    }

    private void setUser() {
        //PerfilUsuariActivityCopia activity = (PerfilUsuariActivityCopia) getActivity();
        Activity activity = getActivity();

        if (mUsuaris == null && activity != null) {
            // This is where we make our REST call to the service. We also pass in our ResultReceiver
            // defined in the RESTResponderFragment super class.

            // We will explicitly call our Service since we probably want to keep it as a private
            // component in our app. You could do this with Intent actions as well, but you have
            // to make sure you define your intent filters correctly in your manifest.
            Intent intent = new Intent(activity, RESTService.class);

            //Obtenir informacio pasada entre activitats
            // Estem en fragment
            int idUsuari = getArguments().getInt("idUsuari");


            // IMPORTANT: canvieu l'URL depenent de si voleu treballar amb el servidor local o el OpenShift
            //intent.setData(Uri.parse("https://project2-pdsudg.rhcloud.com/rest/Usuaris"));
            intent.setData(Uri.parse("http://10.0.2.2:8080/rest/users/"+idUsuari));
            //intent.setData(Uri.parse("http://10.0.2.2:8080/rest/usuaris/"+id));


            // Here we are going to place our REST call parameters. Note that
            // we could have just used Uri.Builder and appendQueryParameter()
            // here, but I wanted to illustrate how to use the Bundle params.

            // Bundle params = new Bundle();

            intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, mReceiver);

            // Here we send our Intent to our RESTService.
            activity.startService(intent);
        }
        else if (activity != null) {
            // Here we check to see if our activity is null or not.
            // We only want to update our views if our activity exists.

        }
    }

    public void onRESTResult(int code, String result) {
        // Here is where we handle our REST response. This is similar to the
        // LoaderCallbacks<D>.onLoadFinished() call from the previous tutorial.

        // Check to see if we got an HTTP 200 code and have some data.
        if (code == 200 && result != null) {
            {
                mUsuaris = getUsuariFromXML(result);
                UpdatePerfil(mUsuaris);
            }
        }
        else {
            Activity activity = getActivity();
            if (activity != null) {
                Toast.makeText(activity, "Failed to load Usuari data. Check your internet settings.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private static UsuariREST getUsuariFromXML(String xml) {
        XStream xstream = new XStream(); // does not require XPP3 library starting with Java 6
        xstream.processAnnotations(UsuariREST.class);
        UsuariREST UsuariList = (UsuariREST) xstream.fromXML(xml);

        return UsuariList;
    }

    private void UpdatePerfil(UsuariREST u)
    {
        Activity a = getActivity();
        if (a!=null)
        {
            TextView tvPerfilEdat = (TextView) a.findViewById(R.id.tvPerfilEdat);
            TextView tvPerfilEmail = (TextView) a.findViewById(R.id.tvPerfilEmail);
            TextView tvPerfilNom = (TextView) a.findViewById(R.id.tvPerfilNom);
            TextView tvPerfilPais = (TextView) a.findViewById(R.id.tvPerfilPais);

            tvPerfilEdat.setText(u.id+"");
            tvPerfilNom.setText(u.alies);
            tvPerfilEmail.setText(u.pass);
            tvPerfilPais.setText("Andorra");

            int visible = getArguments().getInt("invitacio");
            if (visible == 1) a.findViewById(R.id.btnInvite).setVisibility(View.VISIBLE);
            else a.findViewById(R.id.btnInvite).setVisibility(View.GONE);

            String btnText = getArguments().getString("text_btn");
            if (btnText != null)
            {
                Button btn = (Button) a.findViewById(R.id.btnInvite);
                btn.setText(btnText);

                opcioBoto = 1;
            }
        }
        else Toast.makeText(a, "Failed to load Usuari data. Check your internet settings.", Toast.LENGTH_SHORT).show();
    }



    public void acceptarInvitacio()
    {
        Activity a = getActivity();
        Intent intent = new Intent(a, RESTService.class);

        int idUsuari = getArguments().getInt("idUsuari");

        // Build the URL with the appropriate resource path
        intent.setData(Uri.parse(Global.BASE_URL + "/rest/users/addfriend?id=" + idUsuari ));
        // Set the HTTP verb
        intent.putExtra(RESTService.EXTRA_HTTP_VERB, RESTService.GET);

        // Put a receiver in the intent to process the results
        intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, new ResultReceiver(new Handler()) {
            // This function will be called from the REST Service when it invokes the
            // send() command of the ResultReceiver
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultCode == 200) {
                    String xml = resultData.getString(RESTService.REST_RESULT);
                    try {
                        mXstream.processAnnotations(XMLCorrecte.class);
                        XMLCorrecte c = (XMLCorrecte) mXstream.fromXML(xml);
                        if (getActivity()!=null)    {
                            Toast.makeText(getActivity(), "Petició acceptada. "+c.msg, Toast.LENGTH_LONG).show();
                            getActivity().findViewById(R.id.btnInvite).setVisibility(View.GONE);
                        }
                    } catch (Exception ex) {
                        if (getActivity()!=null)    {
                            mXstream.processAnnotations(XMLError.class);
                            XMLError e = (XMLError) mXstream.fromXML(xml);
                            Toast.makeText(getActivity(), "No s'ha acceptat la invitacio. "+e.msg, Toast.LENGTH_LONG).show();
                            /*XMLError e = (XMLError) mXstream.fromXML(xml);
                            mRegister.onError(e);  */
                        }
                    }
                }
            }
        });

        // Here we send our Intent to our RESTService.
        a.startService(intent);
    }

    private void ferPeticioAmistat()
    {
        Activity a = getActivity();
        Intent intent = new Intent(a, RESTService.class);

        int idUsuari = getArguments().getInt("idUsuari");

        // Build the URL with the appropriate resource path
        intent.setData(Uri.parse(Global.BASE_URL + "/rest/users/addinvite?id=" + String.valueOf(idUsuari)));
        // Set the HTTP verb
        intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, RESTService.GET);

        // Put a receiver in the intent to process the results
        intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, new ResultReceiver(new Handler()) {
            // This function will be called from the REST Service when it invokes the
            // send() command of the ResultReceiver
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultCode == 200) {
                    String xml = resultData.getString(RESTService.REST_RESULT);
                    try {
                        //XMLCorrecte e = (XMLCorrecte) mXstream.fromXML(xml);
                        if (getActivity()!=null)    {
                            Toast.makeText(getActivity(), "Petició enviada", Toast.LENGTH_LONG).show();
                            getActivity().findViewById(R.id.btnInvite).setVisibility(View.GONE);
                        }
                    } catch (Exception ex) {
                        if (getActivity()!=null)    {
                            Toast.makeText(getActivity(), "No s'ha pacceptar amistat", Toast.LENGTH_LONG).show();
                            /*XMLError e = (XMLError) mXstream.fromXML(xml);
                            mRegister.onError(e);  */
                        }
                    }
                }
            }
        });

        // Here we send our Intent to our RESTService.
        a.startService(intent);

    }
}
