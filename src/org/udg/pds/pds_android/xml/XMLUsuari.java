package org.udg.pds.pds_android.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: imartin
 * Date: 28/03/13
 * Time: 16:51
 * To change this template use File | Settings | File Templates.
 */
@XStreamAlias("usuari")
public class XMLUsuari {

    public Long id;

    public String alies;

    public String pass;

    public String email;

    @Override
    public String toString() {
        return id + " -> " + alies;
    }
}
