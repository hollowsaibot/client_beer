package org.udg.pds.pds_android.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.adapter.MembersAdapter;
import org.udg.pds.pds_android.fragment.PDSResponderFragmentOriginal;

public class SwitchActivity extends FragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.switch_layout);
    }
}