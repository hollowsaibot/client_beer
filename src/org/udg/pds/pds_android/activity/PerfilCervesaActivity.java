package org.udg.pds.pds_android.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.data.BeerListREST;
import org.udg.pds.pds_android.fragment.*;
import org.udg.pds.pds_android.service.RESTService;
import org.udg.pds.pds_android.util.Dialog;
import org.udg.pds.pds_android.util.Global;
import org.udg.pds.pds_android.xml.XMLCommentList;
import org.udg.pds.pds_android.xml.XMLError;

import java.util.HashMap;
import java.util.Map;

//import org.udg.pds.pds_android.fragment.PDSResponderFragmentPerfilUsuari;

/**
 * Created with IntelliJ IDEA.
 * User: Norbert
 * Date: 19/04/13
 * Time: 01:04
 * To change this template use File | Settings | File Templates.
 */
public class PerfilCervesaActivity extends FragmentActivity
        implements PDSResponderFragmentPerfilCervesa.onRESTPerfilCervesa,
        BeerComments.OnBeerCommentsListener

{
    PDSResponderFragmentPerfilCervesa mResponder;
    BeerDataUpdater bUpdater;
    UserDataUpdater uUpdater;
    BeerComments bl;

    FragmentManager fm;
    FragmentTransaction ft;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.beer_detail);

        fm = getSupportFragmentManager();
        ft = fm.beginTransaction();

        // RESTResponderFragments call setRetainedInstance(true) in their onCreate() method. So that means
        // we need to check if our FragmentManager is already storing an instance of the responder.
        mResponder = (PDSResponderFragmentPerfilCervesa) fm.findFragmentByTag("PDSResponderFragmentPerfilCervesa");
        if (mResponder == null) {
            mResponder = new PDSResponderFragmentPerfilCervesa();
            //this.getIntent().putExtra("idCervesa",1); //TODO: treure
            mResponder.setArguments(this.getIntent().getExtras());

            // We add the fragment using a Tag since it has no views. It will make the Twitter REST call
            // for us each time this Activity is created.
            ft.add(mResponder, "PDSResponderFragmentPerfilCervesa");
        }

        // Make sure you commit the FragmentTransaction or your fragments
        // won't get added to your FragmentManager. Forgetting to call ft.commit()
        // is a really common mistake when starting out with Fragments.
        ft.commit();

        RatingBar rb = (RatingBar) findViewById(R.id.ratingBar);
        rb.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                //To change body of implemented methods use File | Settings | File Templates.
                ratingBar.setRating(Math.round(rating));
                updateRating(Math.round(rating));
            }
        });

        Button btnAddBeer = (Button) findViewById(R.id.btnBeerAdd);
        btnAddBeer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //To change body of implemented methods use File | Settings | File Templates.
                updateBeer((Integer) v.getTag());
            }
        });

        Button btnComment = (Button) findViewById(R.id.btnBeerComments);
        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //To change body of implemented methods use File | Settings | File Templates.
                commentBeer((Integer) v.getTag());
            }
        });
    }

    private void updateRating(int rating)
    {
        //Todo: poder falta us de fm i ft
        bUpdater = new BeerDataUpdater();
        Bundle bundle = new Bundle();
        String valor = ":rating="+rating; //TODO: revisar correctesa
        bundle.putString("query", valor);
        bUpdater.setArguments(bundle);
        bUpdater.update();
    }

    private void updateBeer(int idBeer)
    {
        //Todo: poder falta us de fm i ft
        uUpdater = new UserDataUpdater();
        Bundle bundle = new Bundle();
        bundle.putInt("addBeer", idBeer);
        uUpdater.setArguments(bundle);
        uUpdater.update();
    }

    private void commentBeer(int idBeer)
    {
        Intent i = new Intent(this, ComentarActivity.class);
        i.putExtra("id_beer", idBeer);

        startActivityForResult(i, Global.RQ_ADD_COMMENT);
    }

    //This function is called whenever an Activity launched with startActivityForResult
    // finishes
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data ) {
          if (requestCode == Global.RQ_ADD_COMMENT) {
              if(resultCode == RESULT_OK)
                this.updateBeerComments();
          }
    }

    @Override
    public void onDataReceivedSuccessful(BeerListREST.BeerREST beer) {
        // Once the user has authenticated we can remove the login fragment and add the TaskList one
        fm = getSupportFragmentManager();
        ft = fm.beginTransaction();
        bl = new BeerComments();
        ft.add(R.id.fragment_content_comments, bl, "BeerComments");
        ft.commit();
        this.updateBeerComments();
    }

    @Override
    public void onError(XMLError e) {
        Dialog.onError("Error", this, e, new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
            }
        });
    }

    @Override
    public void updateBeerComments() {
        //To change body of implemented methods use File | Settings | File Templates.
        bl.getComments();
    }

    @Override
    public void onErrorComments(XMLError e) {
        Dialog.onError("Error", this, e, new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
            }
        });
    }
}
