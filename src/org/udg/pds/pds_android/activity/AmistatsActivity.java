package org.udg.pds.pds_android.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.adapter.UsuarisAdapter;
import org.udg.pds.pds_android.data.UsuariListREST;
import org.udg.pds.pds_android.fragment.Amistats;
import org.udg.pds.pds_android.util.Dialog;
import org.udg.pds.pds_android.xml.XMLError;

/**
 * Created with IntelliJ IDEA.
 * User: Norbert
 * Date: 08/05/13
 * Time: 02:18
 * To change this template use File | Settings | File Templates.
 */
public class AmistatsActivity extends FragmentActivity
            implements Amistats.onAmistatsListener
{

    //private UsuarisAdapter uAdapter;
    private Amistats responder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest_service);

        //uAdapter = new UsuarisAdapter(this, R.layout.usuari_layout);

        FragmentManager     fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();


         responder = (Amistats) fm.findFragmentByTag("Amistats");
        if (responder == null) {
            responder = new Amistats();

            responder.setArguments(getIntent().getExtras());
            // We add the fragment using a Tag since it has no views. It will make the Twitter REST call
            // for us each time this Activity is created.
            ft.add(R.id.fragment_content, responder);
        }

        // Make sure you commit the FragmentTransaction or your fragments
        // won't get added to your FragmentManager. Forgetting to call ft.commit()
        // is a really common mistake when starting out with Fragments.
        ft.commit();
    }



    @Override
    public void onError(XMLError e) {
        //To change body of implemented methods use File | Settings | File Templates.
        Dialog.onError("Error", this, e, new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
            }
        });    }

    @Override
    public void updateAmistatList() {
        responder.getAmistats();
    }

    @Override
    public void onAmistatsListUpdate(UsuariListREST ul) {
        responder.showUsuariList(ul);
    }
}
