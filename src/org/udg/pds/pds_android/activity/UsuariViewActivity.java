package org.udg.pds.pds_android.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.fragment.PerfilUsuariFragment;

//import org.udg.pds.pds_android.fragment.PDSResponderFragmentPerfilUsuari;

/**
 * Created with IntelliJ IDEA.
 * User: Norbert
 * Date: 19/04/13
 * Time: 01:04
 * To change this template use File | Settings | File Templates.
 */
public class UsuariViewActivity extends FragmentActivity
{
    PerfilUsuariFragment mResponder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.perfil_usuari);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        // RESTResponderFragments call setRetainedInstance(true) in their onCreate() method. So that means
        // we need to check if our FragmentManager is already storing an instance of the responder.
        mResponder = (PerfilUsuariFragment) fm.findFragmentByTag("PerfilUsuariFragment");
        if (mResponder == null)
        {
            mResponder = new PerfilUsuariFragment();
            mResponder.setArguments(getIntent().getExtras());
            ft.add(mResponder, "PerfilUsuariFragment");
        }

        // Make sure you commit the FragmentTransaction or your fragments
        // won't get added to your FragmentManager. Forgetting to call ft.commit()
        // is a really common mistake when starting out with Fragments.
        ft.commit();
    }
}
