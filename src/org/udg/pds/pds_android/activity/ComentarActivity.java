package org.udg.pds.pds_android.activity;

//import org.udg.pds.pds_android.fragment.PDSResponderFragment;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.fragment.Comentar;
import org.udg.pds.pds_android.fragment.Users;
import org.udg.pds.pds_android.xml.XMLError;


public class ComentarActivity extends FragmentActivity {

    private Comentar responder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comentar);

        Button btnComment = (Button) findViewById(R.id.btnComment);
        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //To change body of implemented methods use File | Settings | File Templates.
                AutoCompleteTextView actComentari = (AutoCompleteTextView) findViewById(R.id.txtComment);
                comentar(actComentari.getText().toString());
            }
        });
    }

    private void comentar(String comentari) {

        FragmentManager     fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        // RESTResponderFragments call setRetainedInstance(true) in their onCreate() method. So that means
        // we need to check if our FragmentManager is already storing an instance of the responder.
        responder = (Comentar) fm.findFragmentByTag("Comentar");
        if (responder == null) {
            getIntent().putExtra("comment",comentari);
            responder = new Comentar();
            responder.setArguments(getIntent().getExtras());
            // We add the fragment using a Tag since it has no views. It will make the Twitter REST call
            // for us each time this Activity is created.
            ft.add(responder,"Comentar");
        }

        // Make sure you commit the FragmentTransaction or your fragments
        // won't get added to your FragmentManager. Forgetting to call ft.commit()
        // is a really common mistake when starting out with Fragments.
        ft.commit();
    }
}