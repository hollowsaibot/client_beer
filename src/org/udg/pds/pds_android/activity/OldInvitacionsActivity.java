package org.udg.pds.pds_android.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.adapter.UsuarisAdapter;
import org.udg.pds.pds_android.data.UsuariListREST;
import org.udg.pds.pds_android.fragment.OldResponderInvitacions;
import org.udg.pds.pds_android.xml.XMLError;

/**
 * Created with IntelliJ IDEA.
 * User: Norbert
 * Date: 08/05/13
 * Time: 02:18
 * To change this template use File | Settings | File Templates.
 */
public class OldInvitacionsActivity extends FragmentActivity
            implements OldResponderInvitacions.onResponderInvitacions
{
    private UsuarisAdapter uAdapter;
    private OldResponderInvitacions responder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest_service);

        uAdapter = new UsuarisAdapter(this, R.layout.usuari_layout);

        FragmentManager     fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        // Since we are using the Android Compatibility library
        // we have to use FragmentActivity. So, we use ListFragment
        // to get the same functionality as ListActivity.
        ListFragment list = new ListFragment();
        ft.add(R.id.fragment_content, list);

        // Let's set our list adapter to a simple ArrayAdapter.
        list.setListAdapter(uAdapter);

        // RESTResponderFragments call setRetainedInstance(true) in their onCreate() method. So that means
        // we need to check if our FragmentManager is already storing an instance of the responder.
         responder = (OldResponderInvitacions) fm.findFragmentByTag("ResponderInvitacions");
        if (responder == null) {
            responder = new OldResponderInvitacions();

            // We add the fragment using a Tag since it has no views. It will make the Twitter REST call
            // for us each time this Activity is created.
            ft.add(responder, "ResponderInvitacions");
        }

        // Make sure you commit the FragmentTransaction or your fragments
        // won't get added to your FragmentManager. Forgetting to call ft.commit()
        // is a really common mistake when starting out with Fragments.
        ft.commit();
    }

    public UsuarisAdapter getUsuarisAdapter() {
        return uAdapter;
    }

    @Override
    public void onError(XMLError e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onInvitacionsListUpdate(UsuariListREST ul) {
       responder.showUsuariList(ul);
    }
}
