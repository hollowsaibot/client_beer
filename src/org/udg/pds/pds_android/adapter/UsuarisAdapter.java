package org.udg.pds.pds_android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.data.UsuariListREST.UsuariREST;


public class UsuarisAdapter extends ArrayAdapter<UsuariREST> {

    int resource;
    String response;
    Context context;

    private LayoutInflater inflater = null;

    //Initialize adapter
    public UsuarisAdapter(Context context, int resource) {
        super(context, resource);
        this.resource=resource;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
     
     
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LinearLayout alertView;
        //Get the current alert object
        UsuariREST m = getItem(position);
         
        View vi = convertView;
        //Inflate the view
        if(convertView==null)
        {
        	vi = inflater.inflate(resource, null);
        }
        //Set a id of user
        vi.setId((int)m.id);
        //Get the text boxes from the listitem.xml file
        //TextView idText =(TextView)vi.findViewById(R.id.usuariViewId);
        TextView idNom =(TextView)vi.findViewById(R.id.userViewNom);
         
        //Assign the appropriate data from our alert object above
        //idText.setText(String.valueOf(m.id));
        idNom.setText(m.alies);

        return vi;
    }
 
}