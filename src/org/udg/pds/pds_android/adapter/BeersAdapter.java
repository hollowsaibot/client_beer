package org.udg.pds.pds_android.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.data.BeerListREST.BeerREST;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;


public class BeersAdapter extends ArrayAdapter<BeerREST> {

    int resource;
    String response;
    Context context;

    private LayoutInflater inflater = null;

    //Initialize adapter
    public BeersAdapter(Context context, int resource) {
        super(context, resource);
        this.resource=resource;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
     
     
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LinearLayout alertView;
        //Get the current alert object
        BeerREST m = getItem(position);
         
        View vi = convertView;
        //Inflate the view
        if(convertView==null)
        {
        	vi = inflater.inflate(R.layout.beer_layout, null);
        }
        //Set a id of beer
        vi.setId((int)m.id);
        //Get the text boxes from the listitem.xml file
        ImageView imgText =(ImageView)vi.findViewById(R.id.beerViewImage);
        TextView idNom =(TextView)vi.findViewById(R.id.beerViewNom);
         
        //Assign the appropriate data from our alert object above
        idNom.setText(m.nom);

        /*try {
            Bitmap bitmap = BitmapFactory.decodeStream((InputStream)new URL(String.valueOf(m.fotografia)).getContent());
            imgText.setImageBitmap(bitmap);
        } catch (MalformedURLException e) {
            //e.printStackTrace();
        } catch (IOException e) {
            //e.printStackTrace();
        }*/
        //imgText.setImageURI(Uri.parse(m.fotografia));

        return vi;
    }
 
}