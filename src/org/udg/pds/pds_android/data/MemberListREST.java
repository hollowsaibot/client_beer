package org.udg.pds.pds_android.data;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("collection")
public class MemberListREST {
	
	@XStreamImplicit
	List<MemberREST> members;

	public List<MemberREST> getMembersList() {
		return members;
	}
	
	@XStreamAlias("member")
	public static class MemberREST {
		public long id;
		public String name;
		public String email;
		@XStreamAlias("phoneNumber")
		public String phone;
		
		@Override
		public String toString() {
			return id + " -> " + email;
		}
	}
}

