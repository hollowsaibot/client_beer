package org.udg.pds.pds_android.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("collection")
public class BeerListREST {
	
	@XStreamImplicit
	List<BeerREST> Beers;

	public List<BeerREST> getBeersList() {
		return Beers;
	}
	
	@XStreamAlias("cervesa")
	public static class BeerREST {
        @XStreamAlias("aspecte")
        public String aspecte;
        @XStreamAlias("fermentacio")
        public String fermentacio;
        @XStreamAlias("fotografia")
        public String fotografia;
        @XStreamAlias("id")
		public long id;
        @XStreamAlias("graduacio")
        public Integer graduacio;
        @XStreamAlias("marca")
        public String marca;
        @XStreamAlias("nom")
        public String nom;
        @XStreamAlias("pais")
        public String pais;

		
		@Override
		public String toString() {
			return id + " -> " + nom;
		}
	}
}

