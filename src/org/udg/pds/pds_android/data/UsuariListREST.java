package org.udg.pds.pds_android.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import java.util.Date;

import java.util.List;

@XStreamAlias("collection")
public class UsuariListREST {
	
	@XStreamImplicit
	List<UsuariREST> Usuaris;

	public List<UsuariREST> getUsuarisList() {
		return Usuaris;
	}
	
	@XStreamAlias("usuari")
	public static class UsuariREST {
        @XStreamAlias("id")
        public long id;
        @XStreamAlias("alies")
		public String alies;
        //@XStreamAlias("dataNaixement")
        //public Date dataNaixement;
        @XStreamAlias("email")
        public String email;
        //@XStreamAlias("idPais")
        //public Integer idPais;
        //@XStreamAlias("nom")
        //public String nom;
        @XStreamAlias("pass")
        public String pass;

		
		@Override
		public String toString() {
			return id + " -> " + alies;
		}
	}
}

